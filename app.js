var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var common = require('./lib/common');
var errorHandler = require('./lib/errorHandler');
var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var devRouter = require('./routes/dev');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.all('*', common.ingress)

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/dev', devRouter);

// catch error to error handler
app.use(errorHandler.errorCatcher);

// error handler
app.use(errorHandler.errorHandle);

module.exports = app;
