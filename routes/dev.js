var express = require('express');
var router = express.Router();



router.get('/:id', function(req, res, next) {
  if (req.params.id == 0) res.send('GET request');
  else next();
}, function (req, res, next) {
  // render a regular page
  res.send('GET request1');
});

router.get('/', function (req, res) {
  res.send('GET request');
});

router.post('/', function (req, res) {
  res.send('POST request');
});

router.put('/', function (req, res) {
  res.send('PUT request');
});  

router.delete('/', function (req, res) {
  res.send('DELETE request');
});  

module.exports = router;  