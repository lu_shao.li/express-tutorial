var express = require('express');
var router = express.Router();


function errorCatcher(err, req, res, next) {
    console.log(err);
   
    next(err);//forward to error handler
}

function errorHandle(err, req, res, next) {
    let errCode = err.status || 500;
    res.status(errCode);
    res.status(errCode).send(err.message);
}


module.exports = {
    errorCatcher: errorCatcher,
    errorHandle: errorHandle
};