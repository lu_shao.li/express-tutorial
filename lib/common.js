var errorHandler = require('./errorHandler');

function ingress(req, res, next) {
    console.log('ingress');
    if (req.headers['authorization']==123) {
        next();
    } else {
        let err = new Error('Unauthorized');
        err.status = 401;
        next(errorHandler.errorCatcher(err, req, res, next));
    }
}

module.exports = {
    ingress: ingress
};
